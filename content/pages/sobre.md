---
title: Sobre
---

Página criada no contexto do oferecimento da disciplina [EE001 (2s2021)](/pages/ee001-2s2021.html) na [FEEC/Unicamp](https://www.fee.unicamp.br/) que foi elaborado em conjunto com estudantes da FEEC (incluindo o [CABS](https://www.cabs.fee.unicamp.br/), o Coletivo das Minas e o Engenho Negro).
